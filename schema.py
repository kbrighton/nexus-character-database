import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import db_session, Companion as CompanionModel, Image as ImageModel, Larp as LarpModel, \
    Medium as MediumModel, Role as RoleModel, Series as SeriesModel, Type as TypeModel, User as UserModel, \
    Character as CharacterModel, Item as ItemModel, Power as PowerModel, Skill as SkillModel, Stat as StatModel


class Companion(SQLAlchemyObjectType):
    class Meta:
        model = CompanionModel
        interfaces = (relay.Node,)


class CompanionConnections(relay.Connection):
    class Meta:
        node = Companion


class Image(SQLAlchemyObjectType):
    class Meta:
        model = ImageModel
        interfaces = (relay.Node,)


class ImageConnections(relay.Connection):
    class Meta:
        node = Image


class Larp(SQLAlchemyObjectType):
    class Meta:
        model = LarpModel
        interfaces = (relay.Node,)


class LarpConnections(relay.Connection):
    class Meta:
        node = Larp


class Medium(SQLAlchemyObjectType):
    class Meta:
        model = MediumModel
        interfaces = (relay.Node,)


class MediumConnections(relay.Connection):
    class Meta:
        node = Medium


class Role(SQLAlchemyObjectType):
    class Meta:
        model = RoleModel
        interfaces = (relay.Node,)


class RoleConnections(relay.Connection):
    class Meta:
        node = Role


class Series(SQLAlchemyObjectType):
    class Meta:
        model = SeriesModel
        interfaces = (relay.Node,)


class SeriesConnections(relay.Connection):
    class Meta:
        node = Series


class Type(SQLAlchemyObjectType):
    class Meta:
        model = TypeModel
        interfaces = (relay.Node,)


class TypeConnections(relay.Connection):
    class Meta:
        node = Type


class User(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        interfaces = (relay.Node,)


class UserConnections(relay.Connection):
    class Meta:
        node = User


class Character(SQLAlchemyObjectType):
    class Meta:
        model = CharacterModel
        interfaces = (relay.Node,)


class CharacterConnections(relay.Connection):
    class Meta:
        node = Character


class Item(SQLAlchemyObjectType):
    class Meta:
        model = ItemModel
        interfaces = (relay.Node,)


class ItemConnections(relay.Connection):
    class Meta:
        node = Item


class Power(SQLAlchemyObjectType):
    class Meta:
        model = PowerModel
        interfaces = (relay.Node,)


class PowerConnections(relay.Connection):
    class Meta:
        node = Power


class Skill(SQLAlchemyObjectType):
    class Meta:
        model = SkillModel
        interfaces = (relay.Node,)


class SkillConnections(relay.Connection):
    class Meta:
        node = Skill


class Stat(SQLAlchemyObjectType):
    class Meta:
        model = StatModel
        interfaces = (relay.Node,)


class StatConnections(relay.Connection):
    class Meta:
        node = Stat


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    all_companions = SQLAlchemyConnectionField(CompanionConnections)
    # Disable sorting over this field
    all_images = SQLAlchemyConnectionField(ImageConnections)
    all_larps = SQLAlchemyConnectionField(LarpConnections)
    all_mediums = SQLAlchemyConnectionField(MediumConnections)
    all_roles = SQLAlchemyConnectionField(RoleConnections)
    all_series = SQLAlchemyConnectionField(SeriesConnections)
    all_types = SQLAlchemyConnectionField(TypeConnections)
    all_users = SQLAlchemyConnectionField(UserConnections)
    all_characters = SQLAlchemyConnectionField(CharacterConnections)
    all_items = SQLAlchemyConnectionField(ItemConnections)
    all_powers = SQLAlchemyConnectionField(PowerConnections)
    all_skills = SQLAlchemyConnectionField(SkillConnections)
    all_stats = SQLAlchemyConnectionField(StatConnections)

    get_users = graphene.Field(User, email=graphene.String())
    get_larps = graphene.Field(Larp, name=graphene.String())
    get_series = graphene.Field(Series, name=graphene.String())

    def resolve_get_users(parent, info, email):
        query = User.get_query(info)
        return query.filter(UserModel.email == email).first()

    def resolve_get_larps(parent, info, name):
        query = Larp.get_query(info)
        return query.filter(LarpModel.name == name).first()

    def resolve_get_series(parent, info, name):
        query = Series.get_query(info)
        return query.filter(SeriesModel.name == name).first()


schema = graphene.Schema(query=Query)
