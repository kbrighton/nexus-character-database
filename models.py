#coding: utf-8
from sqlalchemy import *
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import (scoped_session, sessionmaker, relationship,
                            backref)
from sqlalchemy.ext.declarative import declarative_base
import settings

engine = create_engine(URL(**settings.DATABASE))
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
metadata = Base.metadata
Base.query = db_session.query_property()


class Companion(Base):
    __tablename__ = 'companion'

    id = Column(Integer, primary_key=True, server_default=text("nextval('companion_id_seq'::regclass)"))
    type = Column(String(25))


class Image(Base):
    __tablename__ = 'image'

    id = Column(Integer, primary_key=True, server_default=text("nextval('image_id_seq'::regclass)"))
    filelocation = Column(String(50), nullable=False)
    scale = Column(Float(53))
    translatex = Column(Integer)
    translatey = Column(Integer)


class Larp(Base):
    __tablename__ = 'larps'

    id = Column(Integer, primary_key=True, server_default=text("nextval('larps_id_seq'::regclass)"))
    name = Column(String(30), nullable=False)
    convention = Column(String(30))
    created_at = Column(Time)
    updated_at = Column(Time)
    deleted_at = Column(Time, index=True)

    mediums = relationship('Medium', secondary='larp_mediums')
    users = relationship('User', secondary='user_larps')


class Medium(Base):
    __tablename__ = 'mediums'

    id = Column(Integer, primary_key=True, server_default=text("nextval('mediums_id_seq'::regclass)"))
    medium = Column(String(25), nullable=False)
    updated_at = Column(Time)
    deleted_at = Column(Time)
    created_at = Column(Time)

    seriess = relationship('Series', secondary='series_mediums')


class Role(Base):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True, server_default=text("nextval('roles_id_seq'::regclass)"))
    role = Column(String(30), nullable=False, unique=True)
    created_at = Column(Time)
    updated_at = Column(Time)
    deleted_at = Column(Time)

    users = relationship('User', secondary='user_roles')


class Series(Base):
    __tablename__ = 'series'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_id_seq'::regclass)"))
    name = Column(String(120), nullable=False)
    created_at = Column(Time)
    deleted_at = Column(Time)
    updated_at = Column(Time)
    characters = relationship("Character")


class Type(Base):
    __tablename__ = 'types'

    id = Column(Integer, primary_key=True, server_default=text("nextval('types_id_seq'::regclass)"))
    type = Column(String(25), nullable=False)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, server_default=text("nextval('users_id_seq'::regclass)"))
    email = Column(String(30), nullable=False, unique=True)
    created_at = Column(Time)
    updated_at = Column(Time)
    deleted_at = Column(Time, index=True)


class Character(Base):
    __tablename__ = 'characters'

    id = Column(Integer, primary_key=True, server_default=text("nextval('characters_id_seq'::regclass)"))
    name = Column(String(25), nullable=False)
    bio = Column(Text)
    image1 = Column(ForeignKey('image.id'))
    image2 = Column(ForeignKey('image.id'))
    created_at = Column(Time)
    deleted_at = Column(Time)
    updated_at = Column(Time)
    series_id = Column(ForeignKey('series.id'))

    image = relationship('Image', primaryjoin='Character.image1 == Image.id')
    image3 = relationship('Image', primaryjoin='Character.image2 == Image.id')
    series = relationship('Series')
    items = relationship('Item', secondary='items_characters')
    powers = relationship("Power", backref = "power")
    stats = relationship("Stat", backref = "stat")
    skills = relationship("Skill", backref = "skill")



class Item(Base):
    __tablename__ = 'items'

    id = Column(Integer, primary_key=True, server_default=text("nextval('items_id_seq'::regclass)"))
    name = Column(String(17), nullable=False)
    description = Column(Text, nullable=False)
    series_id = Column(ForeignKey('series.id'))
    larp_id = Column(ForeignKey('larps.id'))
    two_sided = Column(Boolean, nullable=False, server_default=text("false"))
    back_description = Column(Text)
    is_sheet_item = Column(Boolean, nullable=False, server_default=text("false"))
    integrity = Column(Integer)
    holdout = Column(Integer)
    uses = Column(Integer)
    image = Column(ForeignKey('image.id'))
    is_companion = Column(Boolean, server_default=text("false"))
    companion_id = Column(ForeignKey('companion.id'))

    companion = relationship('Companion')
    image1 = relationship('Image')
    larp = relationship('Larp')
    series = relationship('Series')


t_larp_mediums = Table(
    'larp_mediums', metadata,
    Column('larp_id', ForeignKey('larps.id')),
    Column('medium_id', ForeignKey('mediums.id'))
)


t_series_mediums = Table(
    'series_mediums', metadata,
    Column('series_id', ForeignKey('series.id')),
    Column('medium_id', ForeignKey('mediums.id'))
)


t_user_larps = Table(
    'user_larps', metadata,
    Column('larp_id', ForeignKey('larps.id')),
    Column('user_id', ForeignKey('users.id'))
)


t_user_roles = Table(
    'user_roles', metadata,
    Column('user_id', ForeignKey('users.id'), nullable=False),
    Column('role_id', ForeignKey('roles.id'), nullable=False)
)


t_items_characters = Table(
    'items_characters', metadata,
    Column('item_id', ForeignKey('items.id')),
    Column('character_id', ForeignKey('characters.id'))
)


class Power(Base):
    __tablename__ = 'powers'

    id = Column(Integer, primary_key=True, server_default=text("nextval('powers_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    skill = Column(String(15))
    cost = Column(Integer)
    description = Column(Text, nullable=False)
    based_on = Column(Integer)
    created_at = Column(Time)
    deleted_at = Column(Time)
    updated_at = Column(Time)
    character_id = Column(ForeignKey('characters.id'))
    is_latest = Column(Boolean, server_default=text("false"))

    character = relationship('Character')
    types = relationship('Type', secondary='power_types')


class Skill(Base):
    __tablename__ = 'skills'

    id = Column(Integer, primary_key=True, server_default=text("nextval('skills_id_seq'::regclass)"))
    character_id = Column(ForeignKey('characters.id'))
    acting = Column(Integer)
    computer_use = Column(Integer)
    deception = Column(Integer)
    influence = Column(Integer)
    magic_chi = Column(Integer)
    medicine = Column(Integer)
    pilot_vehicle = Column(Integer)
    science = Column(Integer)
    yoink = Column(Integer)
    authority = Column(Integer)
    hide_stuff = Column(Integer)
    disguise = Column(Integer)
    jump = Column(Integer)
    mechanics = Column(Integer)
    pilot_mecha = Column(Integer)
    run = Column(Integer)
    search = Column(Integer)
    stealth = Column(Integer)
    archery = Column(Integer)
    bfg = Column(Integer)
    blades = Column(Integer)
    blunt = Column(Integer)
    dodge = Column(Integer)
    hand_to_hand = Column(Integer)
    marksmanship = Column(Integer)
    mecha_weaponry = Column(Integer)
    special_weapons = Column(Integer)
    throw = Column(Integer)
    authority_type = Column(String(50))
    is_latest = Column(Boolean, server_default=text("false"))

    character = relationship('Character')


class Stat(Base):
    __tablename__ = 'stats'

    id = Column(Integer, primary_key=True, server_default=text("nextval('stats_id_seq'::regclass)"))
    power = Column(Integer, server_default=text("1"))
    power_alt_name = Column(String(15))
    athletics = Column(Integer, server_default=text("1"))
    athletics_alt_name = Column(String(15))
    brains = Column(Integer, server_default=text("1"))
    brains_alt_name = Column(String(15))
    confidence = Column(Integer, server_default=text("1"))
    confidence_alt_name = Column(String(15))
    charisma = Column(Integer, server_default=text("1"))
    charisma_alt_name = Column(String(15))
    affluence = Column(Integer, server_default=text("0"))
    affluence_alt_name = Column(String(15))
    character_id = Column(ForeignKey('characters.id'))
    body = Column(Integer, nullable=False)
    energy = Column(Integer, nullable=False)
    resilience = Column(Integer)
    is_latest = Column(Boolean, server_default=text("false"))

    character = relationship('Character')


t_power_types = Table(
    'power_types', metadata,
    Column('power_id', ForeignKey('powers.id')),
    Column('type_id', ForeignKey('types.id'))
)


